from django.db import models

class User(models.Model):
	"""Usuario"""
	name = models.CharField(max_length=50)
	email = models.CharField(max_length=50)

	def __str__(self):
		return '%s' %(self.id)


class New (models.Model):
	"""Noticias"""
	user_id = models.ForeignKey('User',on_delete = models.CASCADE,)
	tittle = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	text = models.TextField(max_length=2000)
	
	def __str__(self):
		return '%s %s %s' %(self.tittle,self.user_id,self.pub_date)

class Comment (models.Model):
	"""Comentarios"""
	new_id = models.ForeignKey('New',on_delete = models.CASCADE,related_name='comment')
	pub_date = models.DateTimeField('date published')
	user_id = models.ForeignKey('User',on_delete = models.CASCADE,)
	text_com = models.TextField(max_length=1000)

	class Meta:
		unique_together = ('id',)
		ordering = ['id']

	def __str__(self):
		return '%s %s' %(self.text_com,self.user_id)


class Like (models.Model):
	"""Likes"""
	new_id = models.ForeignKey('New',on_delete = models.CASCADE,related_name='like')
	pub_date = models.DateTimeField('date published')
	user_id = models.ForeignKey('User',on_delete = models.CASCADE,)

	class Meta:
		unique_together = ('id',)
		ordering = ['id']

	def __str__(self):
		return '%s' %(self.user_id) 

class Image (models.Model):
	"""Imagenes"""
	new_id = new_id = models.ForeignKey('New',on_delete = models.CASCADE,related_name='image')
	piture = models.CharField(max_length=2000)


	class Meta:
		unique_together = ('id',)
		ordering = ['id']

	def __str__(self):
		return '%s %s'%(self.id,self.piture_thumbnail)

