from rest_framework import generics
from .models import *
from .serializers import *
from django.shortcuts import get_object_or_404

class NewList(generics.ListCreateAPIView):
	queryset = New.objects.all()
	serializer_class = NewSerializer

class NewDetail(generics.ListCreateAPIView):
	serializer_class = NewSerializer
	def get_queryset(self):
		queryset = New.objects.filter(pk=self.kwargs['pk'])
		return queryset

class CommentList(generics.ListCreateAPIView):
	queryset = Comment.objects.all()
	serializer_class = CommentSerializer

	def get_object(self):
		queryset = self.get_queryset()
		obj = get_object_or_404(
			queryset,
			pk=self.kwargs['pk'],
		)
		return obj

class LikeList(generics.ListCreateAPIView):
	queryset = Like.objects.all()
	serializer_class = LikeSerializer

	def get_object(self):
		queryset = self.get_queryset()
		obj = get_object_or_404(
			queryset,
			pk=self.kwargs['pk'],
		)
		return obj