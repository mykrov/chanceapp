from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from news.views import *


urlpatterns = [
	url(r'^news/$',NewList.as_view(),name='news'),
	url(r'^news/(?P<pk>\w+)$',NewDetail.as_view(),name='new_detail'),
	url(r'^news/comment/$',CommentList.as_view(),name='comment'),
	url(r'^news/like/$',LikeList.as_view(),name='like'),
	#url(r'^news/(?P<pk>\w+)$', NewList.as_view(),name='news'),
	# url(r'^/comment',CommentList.as_view(), name='comment'),
]

urlpatterns = format_suffix_patterns(urlpatterns)