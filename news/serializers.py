from news.models import *
from rest_framework import serializers



class UserSerializer(serializers.ModelSerializer):
	class Meta: 
		model = User
		fields = ('__all__')

class CommentSerializer(serializers.ModelSerializer):
	class Meta: 
		model = Comment
		fields = ('__all__')

class LikeSerializer(serializers.ModelSerializer):
	class Meta: 
		model = Like
		fields = ('__all__')

class ImageSerializer(serializers.ModelSerializer):
	class Meta:
		model = Image
		fields = ('__all__')

class NewSerializer(serializers.ModelSerializer):

	comment = CommentSerializer(many=True,read_only=True)
	like = LikeSerializer(many=True,read_only=True)
	image = ImageSerializer(many=True,read_only=True)
	
	class Meta: 
		model = New
		fields = ('id','user_id','tittle','pub_date','text','image','like','comment')



	