
from django.conf.urls import url, include
from django.conf import	settings
from django.contrib import admin
from rest_framework.authtoken import views
from news.views import *
from news.urls import url

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('news.urls', namespace='news')),

]
